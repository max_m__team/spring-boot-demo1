

var temperatureGetter = function() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/api/v1/temperature/avg', true);
    xhr.send();

    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            // alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var response = JSON.parse(xhr.responseText);
            var tempDIV = document.getElementsByClassName('avg-temperature');
            tempDIV[0].innerHTML = 'Average temperature for past hour is ' + response.averageTemperatureForPastHour + ' celsius degrees';
        }

    }
};

setInterval(temperatureGetter, 5000);