package com.app1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.app1.config.Configuration;


@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
@ComponentScan(value = {"com.app1.entity",
        "com.app1.repository", "com.app1.rest",
        "com.app1.dto", "com.app1.schedule", "com.app1.config."})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(
                new Object[]{
                        Application.class,
                        Configuration.class,
                },
                args);
    }
}
