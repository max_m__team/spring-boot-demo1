package com.app1.schedule;

import com.app1.entity.Temperature;
import com.app1.repository.TemperatureRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Component
public class JobScheduler {

    @Autowired
    private TemperatureRepository temperatureRepository;

    private Random random = new Random(123454321);

    @Scheduled(fixedRate = 10000)
    public void addNewTemperatureToDB() {
        Temperature temperature = new Temperature();
        temperature.setTemperatureCelsius(random.nextDouble()* 100);
        temperature.setTimestamp(Calendar.getInstance().getTime());
        temperatureRepository.save(temperature);
    }

}
