package com.app1.dto;

public class TemperatureDTO {

    private Double averageTemperatureForPastHour;

    public TemperatureDTO(Double averageTemperatureForPastHour) {
        this.averageTemperatureForPastHour = averageTemperatureForPastHour;
    }

    public Double getAverageTemperatureForPastHour() {
        return averageTemperatureForPastHour;
    }

    public void setAverageTemperatureForPastHour(Double averageTemperatureForPastHour) {
        this.averageTemperatureForPastHour = averageTemperatureForPastHour;
    }
}
