package com.app1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app1.dto.TemperatureDTO;
import com.app1.repository.TemperatureRepository;

import java.sql.Date;
import java.util.Calendar;

@RestController
@RequestMapping("/api/v1/temperature")
public class TemperatureStatisticService {

    @Autowired
    private TemperatureRepository temperatureRepository;

    @RequestMapping(
            path = "/avg",
            produces = "application/json; charset=UTF-8",
            method = RequestMethod.GET)
    public @ResponseBody TemperatureDTO getAverageTemperatureForPastHour() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -1);
        Date bottomLimitDatetime = new Date(calendar.getTimeInMillis());
        return temperatureRepository.getAverageTemperature(bottomLimitDatetime);
    }
}
