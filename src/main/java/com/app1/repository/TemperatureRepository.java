package com.app1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app1.dto.TemperatureDTO;
import com.app1.entity.Temperature;

import java.util.Date;

@Repository
public interface TemperatureRepository extends JpaRepository<Temperature, Date> {

    @Query("SELECT new com.app1.dto.TemperatureDTO(" +
            "   AVG(t.temperatureCelsius) as averageTemperatureForPastHour )  " +
            "FROM Temperature t " +
            "WHERE t.timestamp > :bottomLimitTime"
    )
    TemperatureDTO getAverageTemperature(@Param("bottomLimitTime") Date bottomLimitTime);
}
