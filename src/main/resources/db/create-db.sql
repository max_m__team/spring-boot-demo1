-- DROP TABLE IF EXISTS `temperature`;

CREATE TABLE `temperature` (
  `timestamp`             TIMESTAMP PRIMARY KEY ,
  `temperature_celsius`   DOUBLE
);